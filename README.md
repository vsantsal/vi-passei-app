ViPassei App
============

Repositório que centraliza as referências aos repositórios (independentes) dos serviços que compõem a plataforma `Vi Passei`, *streaming* de vídeos permitindo o gerenciamento e exibição de vídeoaulas, focado em conteúdo de aulas de matérias cobradas em concursos públicos (*direito administrativo*, *direito constitucional*, *raciocínio lógico e matemático* etc).

O link no gitlab é https://gitlab.com/vsantsal/vi-passei-app.


